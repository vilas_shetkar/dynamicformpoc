import { Injectable } from '@angular/core';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class DynamicFormService {

  constructor() { }

  private formComponent: DynamicFormComponent;

  setFormComponent(formComponent: DynamicFormComponent) {
    this.formComponent = formComponent;
  }

}
