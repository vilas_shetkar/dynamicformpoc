import { Component } from '@angular/core';
import { DynamicFormService } from './dynamic-form.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(public formService: DynamicFormService){

  }

  model: any = {
    one: "no"
  };

  elements: any = [{
    controlType : 'radio',
    description: 'Do You want to tell your name?',
    required: true,
    fieldId: 'one',
    options: [{ key: 'yes', value: 'Yes'}, { key: 'no', value: 'No'}]
  }, {
    controlType : 'text',
    description: 'What is your name?',
    required: true,
    fieldId: 'two'
  }, {
    controlType : 'text',
    description: 'What is your city?',
    required: false,
    fieldId: 'three'
  }]

  layout: any = [{
    type: 'element',
    element: 'one',
    elementClass: 'parent-class row',
    inputClass: 'form-control',
    onChange(value, control, formComponent){
      console.log(value, control, formComponent);
      if(value == 'no'){
        formComponent.removeControl('three');
      }else{
        formComponent.addControl('three');
      }
    }
  }, {
    type: 'section',
    elementClass: 'parent-class row',
    items: [{
      element: 'two',
      elementClass: 'col-6',
      condition: (element, formGroup, model) => {
        console.log(element, formGroup, model);
        return true;
      }
    }, {
      element: 'three',
      elementClass: 'col-6'
    }]
  }]
  
}
