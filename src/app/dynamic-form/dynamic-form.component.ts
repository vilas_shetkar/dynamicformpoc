import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DynamicFormService } from '../dynamic-form.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {

  @Input() elements: any[];
  @Input() layout: any[];
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() onChanges: EventEmitter<any> = new EventEmitter<any>();
  form: FormGroup;
  formControls: any = {};
  formControlsBackup: any = {};

  constructor(private formService: DynamicFormService) { 
    this.formService.setFormComponent(this);
  }

  ngOnInit() {
    console.log(this.elements, this.layout, this.onSubmit, this.onChanges);

    this.elements.forEach((item, index) => {
      this.setFormControl(item, index);
    });

    this.formControlsBackup = Object.assign({}, this.formControls);

    this.form = new FormGroup(this.formControls);

    console.log(this.form);

    this.applyFieldModifiers(this.layout);
    
  }

  
  removeControl(controlName){
    this.form.removeControl(controlName);
  }

  addControl(controlName){
    if(this.formControlsBackup[controlName]){
      console.log(this.formControlsBackup[controlName]);
      this.form.addControl(controlName, this.formControlsBackup[controlName]);
    }
  }

  setFormControl(item, index){
    this.formControls[item.fieldId] = new FormControl();
    this.formControls[item.fieldId].fieldIndex = index;
    this.formControls[item.fieldId].elementOption = item;
  }


  applyFieldModifiers(layout: any[]){
    layout.forEach((item, index) => {
      if(this.formControls[item.element]){
        this.formControls[item.element].layoutNode = item;
      }

      if(item.type == 'section'){
        return this.applyFieldModifiers(item.items);
      }

      if(typeof item.condition === 'function'){
        this.checkCondition(item, this.formControls[item.element])
      }
      
      if(typeof item.onChange === 'function'){
        this.onElementChange(item, this.formControls[item.element])
      }

      if(typeof item.validator === 'function'){
        this.applyValidator(item, this.formControls[item.element])
      }

      if(item.validators){
        this.applyValidators(item, this.formControls[item.element])
      }

    });
  }

  onElementChange(layoutNode, formControl){
    formControl.valueChanges.subscribe(value => {
      console.log(layoutNode, formControl);
      layoutNode.onChange(value, formControl, this);
    });
  }
  
  checkCondition(layoutNode, formControl){
    console.log(layoutNode, formControl);
    const keepField = layoutNode.condition(formControl, this.form);
    console.log(keepField, formControl);
    if(!keepField){
      this.form.removeControl(layoutNode.element);
    }
    
  }

  applyValidator(layoutNode, element){
    console.log('applyValidator-', layoutNode, element);
  }

  applyValidators(layoutNode, element){
    console.log('applyValidators...', layoutNode, element);
  }

}
