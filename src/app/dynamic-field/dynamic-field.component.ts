import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dynamic-field',
  templateUrl: './dynamic-field.component.html',
  styleUrls: ['./dynamic-field.component.scss']
})
export class DynamicFieldComponent implements OnInit {

  @Input() element: any[];
  @Input() formGroup: FormGroup;
  @Output() onChange: EventEmitter<any> = new EventEmitter<any>();
  
  formControl: AbstractControl;

  constructor() {

  }

  ngOnInit() {
    console.log(this.element, this.onChange, this.formGroup);
  }

}
